var gulp = require('gulp');
var gp_sass = require('gulp-sass');
var gp_uglify = require('gulp-uglify');
var gp_concat = require('gulp-concat');
var gp_rename = require('gulp-rename');
var gp_copy = require('gulp-copy');
var gp_fileinclude = require('gulp-file-include');
var gp_webserver = require('gulp-webserver');
var gp_awspublish = require('gulp-awspublish');
var gp_stripdebug = require('gulp-strip-debug');
var isDebug = true;

function srcForScssLibs(isDebug) {
    var paths = [];

    // normalize-css
    paths.push('bower_components/_normalize.scss/');

    return paths;
}

gulp.task('styles', function() {
    var includePaths = srcForScssLibs();
    includePaths.push('./src/styles');
    gulp.src('src/styles/main.scss')
        .pipe(gp_sass({includePaths: includePaths, errLogToConsole: true}).on('error', gp_sass.logError))
        .pipe(gulp.dest('./dist/css/'));
    gulp.src('src/styles/ie.scss')
        .pipe(gp_sass({includePaths: includePaths, errLogToConsole: true}).on('error', gp_sass.logError))
        .pipe(gulp.dest('./dist/css/'));
});

function srcForJsLibs(isDebug) {
    var sources = [];
    var basePath;

    // jquery
    basePath = 'bower_components/jquery/dist/';
    sources.push(basePath + 'jquery.js');

    // gsap

    //basePath = 'bower_components/gsap/src/minified/';
    //sources.push(basePath + 'easing/EasePack.min.js');
    //sources.push(basePath + 'plugins/CSSPlugin.min.js');
    //sources.push(basePath + 'plugins/AttrPlugin.min.js');
    //sources.push(basePath + 'TweenLite.min.js');
    //sources.push(basePath + 'jquery.gsap.min.js');

    basePath = 'bower_components/gsap/src/uncompressed/';
    sources.push(basePath + 'easing/EasePack.js');
    sources.push(basePath + 'plugins/CSSPlugin.js');
    sources.push(basePath + 'plugins/AttrPlugin.js');
    sources.push(basePath + 'TweenLite.js');
    sources.push(basePath + 'jquery.gsap.js');


    // ScrollMagic
    basePath = 'bower_components/ScrollMagic/scrollmagic/uncompressed/';
    sources.push(basePath + '*.js');
    sources.push(basePath + 'plugins/animation.gsap.js');
    if (isDebug) {
        sources.push(basePath + 'plugins/debug.addindicators.js');
    }

    return sources;
}

gulp.task('webserver', function() {
  gulp.src('./dist')
    .pipe(gp_webserver({
      //host: "192.168.100.20",
      host: "localhost",
      livereload: true,
      open: true
    }));
});

gulp.task('js_libs', function() {
  var sources = srcForJsLibs(true);
  return gulp.src(sources)
    .pipe(gp_concat('libs.js'))
//    .pipe(gp_uglify())
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('js_libs_publish', function() {
  var sources = srcForJsLibs(true);
  return gulp.src(sources)
    .pipe(gp_concat('libs.js'))
    .pipe(gp_uglify())
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('js', function() {
  return gulp.src(['src/js/lib/**/*.js', 'src/js/*.js'])
    .pipe(gp_concat('main.js'))
//    .pipe(gp_uglify())
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('js_publish', function() {
  return gulp.src(['src/js/lib/**/*.js', 'src/js/*.js'])
    .pipe(gp_concat('main.js'))
    .pipe(gp_stripdebug())
    .pipe(gp_uglify())
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('html', function() {
  gulp.src(['src/index.html'])
    .pipe(gp_fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('fonts', function() {
   gulp.src('./src/fonts/**/*.{ttf,woff,eof,svg,woff2}')
   .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('images', function() {
   gulp.src('./src/images/**/*.{jpg,png,gif}')
   .pipe(gulp.dest('./dist/images'));
});

gulp.task('publish', function() {

  // create a new publisher using S3 options
  // http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#constructor-property
  var publisher = gp_awspublish.create({
    params: {
      Bucket: '46run.com',
      Key: 'AKIAIZFVPGL2DXDSAGYA',
      Secret: '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'
    }
  });

  // define custom headers
  var headers = {
    'Cache-Control': 'max-age=315360000, no-transform, public'
    // ...
  };

  return gulp.src('./dist/*.*')
     // gzip, Set Content-Encoding headers and add .gz extension
    .pipe(gp_awspublish.gzip({ ext: '.gz' }))

    // publisher will add Content-Length, Content-Type and headers specified above
    // If not specified it will set x-amz-acl to public-read by default
    .pipe(publisher.publish(headers))

    // create a cache file to speed up consecutive uploads
    .pipe(publisher.cache())

     // print upload updates to console
    .pipe(gp_awspublish.reporter());
});


gulp.task('watch', ['debug', 'webserver'], function() {
  gulp.watch(['src/styles/**/*.scss'], ['styles']);
  gulp.watch(['src/js/**/*.js'], ['js']);
  gulp.watch(['src/**/*.html'], ['html']);
});

gulp.task('debug', ['styles', 'js_libs', 'js', 'html', 'fonts', 'images'],
    function() {
        console.log("All done");
});


gulp.task('default', ['styles', 'js_libs_publish', 'js_publish', 'html', 'fonts', 'images'],
    function() {
        console.log("All done");
});
