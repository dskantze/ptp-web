var Dispatcher = function() {
    this.listeners = {};

    this.addListener = function(name, callback) {
        if (!this.listeners[name]) {
            this.listeners[name] = [];
        }
        this.listeners[name].push(callback);
    };

    this.removeListener = function(name, callback) {
        var i = 0, l;
        if (this.listeners[name]) {
            while (this.listeners[name].length > 0) {
                l = this.listeners[name][i];
                if (callback === l) {
                    this.listeners.splice(i, 1);
                    break;
                }
                i++;
            }
        }
    };

    this.dispatch = function(name, params) {
        var i = 0, l;
        if (this.listeners[name]) {
            for (i = 0; i < this.listeners[name].length; i++) {
                l = this.listeners[name][i];
                l.apply(null, params);
            }
        }
    };
}