var VideoMonitor = function(player, monitorInterval, skipCallbacksInSeek) {
    var lastTime = 0,
        callbackInterval = null,
        updateInterval = monitorInterval;

    this.player = player;
    this.cuepoints = [];
    this.skipCallbacksInSeek = (skipCallbacksInSeek ? true : false); // force this into being bool

    if (!player) {
        throw new Error('player not set');
    }
    if (!updateInterval) {
        throw new Error('monitorInterval not set');
    }

    this.__getCallbacks = function(time) {
        var i, callbacks = [], j;
        for (i = 0; i < this.cuepoints.length; i++) {
            if (this.cuepoints[i].time === time) {
                callbacks.push[this.cuepoints[i].callback];
            } else if (time > this.cuepoints[i].time) {
                break;
            }
        }
        return callbacks;
    };

    this.__fireCallbacksBetweenTimes = function(startTime, endTime) {
        var i, cuepoint;
        for (i = 0; i < this.cuepoints.length; i++) {
            cuepoint = this.cuepoints[i];
            if (cuepoint.time > startTime && cuepoint.time <= endTime) {
                cuepoint.callback();
            } else if (cuepoint.time > endTime) {
                return;
            }
        }
    };

    this.__orderCuepoints = function() {
        var sortFn = function(a, b) { return a.time - b.time; }
        this.cuepoints.sort(sortFn);
    };

    this.addCuepoint = function(time, callback) {
        var i,
            cuepoint = {time: time, callback: callback};
        for (i = 0; i < this.cuepoints.length; i++) {
            if (this.cuepoints[i].time > time) {
                break;
            }
        }
        if (i < this.cuepoints.length) {
            this.cuepoints[i].splice(i, 0, cuepoint);
        } else {
            this.cuepoints.push(cuepoint);
        }
    };

    this.removeCuepoint = function(time, callback) {
        var i;
        while (i < this.cuepoints.length) {
            if (this.cuepoints[i].time === time) {
                this.cuepoints.splice(i, 1);
                continue;
            } else if (this.cuepoints[i].time > time) {
                break;
            }
            i++;
        }
    };

    this.isMonitoring = function() {
        return callbackInterval !== null;
    }

    this.setMonitorInterval = function(newInterval) {
        this.updateInterval = newInterval;
        if (this.isMonitoring()) {
            this.suspend();
            this.start();
        }
    };

    this.getMonitorInterval = function() {
        return updateInterval;
    };

    this.start = function() {
        var me = this;
        console.log("VideoMonitor -- will start");
        if (this.isMonitoring()) {
            return;
        }
        setInterval(function() {
            var now = player.getCurrentTime(),
                delta = (now - lastTime);
            if (delta >= 0) { // we cannot fire cuepoints when we go backwards
                if (!me.skipCallbacksInSeek || (delta * 1000) < updateInterval * 2) { // we add some tolerance to avoid potential inaccuracies in player positions
                    me.__fireCallbacksBetweenTimes(lastTime, now);
                }
            }
            lastTime = now;
        }, updateInterval);
    };

    this.suspend = function() {
        if (this.isMonitoring()) {
            clearInterval(callbackInterval);
        }
    };
}