
var __globalDispatcher = new Dispatcher();

function initYoutubeApi() {
    var tag = document.createElement('script'),
        firstScriptTag;

    tag.src = "https://www.youtube.com/iframe_api";
    firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

function onYouTubeIframeAPIReady() {
    __globalDispatcher.dispatch('video-api-ready');
}

$(function() { // wait for document ready
    // init
    var controller = new ScrollMagic.Controller({
            globalSceneOptions: {
                triggerHook: 'onLeave'
            }
        }),
        videoMonitor = null,
        SCENES,
        player;

    function onPageChange (event) {
        var scene = event.target,
            pageNo,
            pageName;

        for (pageNo = 0; pageNo < SCENES.length; pageNo++) {
            if (SCENES[pageNo] === scene) {
                break;
            }
        }

        pageName = $(scene.triggerElement()).data('page');

        updateNavigation(pageNo);
        //console.log(pageNo, pageName);
        switch (pageName) {
            case "what":
                break;
            case "how":
                if (player)
                    player.pauseVideo();
                break;
        }
    }

    function onScrollChange (event) {
        if (SCENES) {
            var videoScene = SCENES[0];
            if (player && event.target === videoScene && event.progress === 0) {
                $($(videoScene.triggerElement()).addClass('video-loaded'));
                videoMonitor.start();
                player.playVideo();
                player.__isPaused = false;
            } else if (player && !player.__isPaused) {
                videoMonitor.suspend();
                player.pauseVideo();
                player.__isPaused = true;
            }
        }
    }

    function updateNavigation (pageNo) {
        var i, s;
        for (i = 0; i < SCENES.length; i++) {
            s = SCENES[i];
            //if (s.getProgress())
        }
        $("nav.pager > ul > li.active").removeClass('active');
        $("nav.pager > ul > li:eq(" + pageNo + ")").addClass('active');
    }

    function initNavigation () {
        $('nav.pager li').on('click', function(evt) {
            var idx = $(evt.target).index();
            controller.scrollTo(SCENES[idx]);
        });
    }

    function initLightbox() {
        var lightboxContainer = $('#lightbox-container'),
            imgDest = lightboxContainer.find('img');

        function onCloseLightbox(evt) {
            lightboxContainer.animate({'opacity': 0.0}, 300, function() {
                lightboxContainer.css('display', 'none');
            });
        }

        function onTriggerLightbox(evt) {
            var imgSrc = $(evt.target);
            imgDest.attr('src', imgSrc.attr('src'));
            lightboxContainer.css('display', 'table');
            lightboxContainer.css('opacity', '0.0');
            lightboxContainer.animate({'opacity': 1.0}, 300);
        }

        lightboxContainer.click(onCloseLightbox);
        imgDest.click(onCloseLightbox);
        $('img.lb').click(onTriggerLightbox);
    }

    function initEml () {
        var k = ['<', 'a', ' h', 'r', 'e', 'f=', '"m', 'a', 'i', 'l', 't', 'o', ':'].join('') +
                                    "moc.nur64_jak".split("").reverse().join("").replace("_", "@") + '">' +
                                    "moc.nur64_jak".split("").reverse().join("").replace("_", "@") +
                                    ['<', '/', 'a', '>'].join("");
            d = ['<', 'a', ' h', 'r', 'e', 'f="', 'm', 'a', 'i', 'l', 't', 'o', ':'].join('') +
                                    "moc.nur64_leinad".split("").reverse().join("").replace("_", "@") + '">' +
                                    "moc.nur64_leinad".split("").reverse().join("").replace("_", "@") +
                                    ['<', '/', 'a', '>'].join("");
            $('.eml.kaj').html(k);
            $('.eml.daniel').html(d);
    }

    function onWindowResizeLightbox () {
        var lightboxContainer = $('#lightbox-container'),
            imgContainer = lightboxContainer.find('.img'),
            pad = parseInt(imgContainer.css('padding-left').split('px')[0]);
        lightboxContainer.css('height', window.innerHeight + "px");
        lightboxContainer.css('width', window.innerWidth + "px");
        imgContainer.css('height', (window.innerHeight - pad * 2) + "px");
        imgContainer.css('width', (window.innerWidth - pad * 2) + "px");
    }

    function onWindowResizeVideo () {
        var i, s, videoScale, w, h, cssParams, SAR = 1.5, AR = 672/272; // 16 / 9
        controller.update(true);
        for (i = 0; i < SCENES.length; i++) {
            s = SCENES[i];
            s.duration(window.innerHeight);
            $(s.triggerElement()).children('.panel-container').css('height', window.innerHeight + "px");
            s.refresh();
            s.remove();
            s.addTo(controller);
        }
        if ((window.innerHeight * AR) > window.innerWidth) {
            w = Math.round(SAR * window.innerHeight * AR);
            h = Math.round(SAR * window.innerHeight);
        } else {
            w = Math.round(SAR * window.innerWidth);
            h = Math.round(SAR * window.innerWidth / AR);
        }
        cssParams = {
            'width': w + "px",
            'height': h + "px",
            'left': Math.round((window.innerWidth - w) / 2) + "px",
            'top': Math.round((window.innerHeight - h) / 2) + "px"
        };
        console.log(cssParams);
        $(".video-container").css(cssParams);
        return true;
    }

    function onWindowResize() {
        onWindowResizeLightbox();
        onWindowResizeVideo();
        return true;
    }

    function createScenes() {
        // get all slides
        var slides = $("section.panel"),
            scenes = [],
            s,
            tween;

        // create scene for every slide

        for (var i = 0; i < slides.length; i++) {
            tween = null;
            switch (i) {
                case 0:
                    tween = new TweenLite({}, 0.5, { paused: true, onComplete: function() { console.log("trigger 1"); } });
                    break;
                case 1:
                    tween = new TweenLite({}, 0.5, { paused: true, onComplete: function() { console.log("trigger 2"); } });
                    break;
                case 2:
                    tween = new TweenLite({}, 0.5, { paused: true, onComplete: function() { console.log("trigger 3"); } });
                    break;
                case 3:
                    tween = new TweenLite({}, 0.5, { paused: true, onComplete: function() { console.log("trigger 4"); } });
                    break;
                case 4:
                    tween = new TweenLite({}, 0.5, { paused: true, onComplete: function() { console.log("trigger 4"); } });
                    break;
            }
            $(slides[i]).data('pageNo', i);
            s = new ScrollMagic.Scene({
                triggerElement: slides[i]
                ,duration: 100
            });
            s.setTween(tween, 1, {});
            s.on('enter', onPageChange)
            .on('progress', onScrollChange)
            //.setPin(slides[i])
            //.addIndicators() // add indicators (requires plugin)
            .addTo(controller);
            scenes.push(s);
        }
        return scenes;
    }
    if (!isMobile()) {

        $('body').addClass('advanced-scrolling');

        SCENES = createScenes();

        initEml();

        controller.scrollTo(function (newScrollPos) {
            $("html, body").animate({scrollTop: newScrollPos});
        });

        $(window).smartresize(function(evt) { return onWindowResize(); });
        onWindowResize();

        initYoutubeApi();
        initNavigation();
        initLightbox();

        $('.footer.next').click(function(evt) {
            var currentSection = $(evt.target).closest('.panel')[0],
                idx = -1;

            $('.panel').each(function(i, item) {
                if (item === currentSection) {
                    idx = i;
                    return false;
                }
            });
            if (idx >= 0) {
                controller.scrollTo(SCENES[idx + 1]);
            }
        });

        __globalDispatcher.addListener('video-api-ready', function() {
            var startPoint = mmssToSeconds('1.11'),
                loopPoint = mmssToSeconds('5.18');

            player = new YT.Player('player', {
                height: '100%',
                width: '100%',
                playerVars: { 'autoplay': 1, 'controls': 0, 'disablekb': 1, 'enablejsapi': 1, 'loop': 1, 'start': startPoint },
                videoId: '6F7T1EGSoMQ',
                events: {
                    'onReady': function() {
                        player.mute();
                    },
                    'onStateChange': function(event) {
                        if (event.data === YT.PlayerState.PLAYING) {
                            if (!videoMonitor) {
                                videoMonitor = new VideoMonitor(player, 1000);
                                    videoMonitor.addCuepoint(loopPoint, function() {
                                        player.seekTo(startPoint);
                                });
                                videoMonitor.start();
                                $('.background-video').addClass('video-loaded');
                            }
                        }
                        console.log('onStateChange');
                    }
                }
            });
        });
    } else {
        $('body').addClass('advanced-scrolling-disabled');
    }
});